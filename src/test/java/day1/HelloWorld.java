package day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello World");
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(); // driver is a chrome driver	
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String url = "https://www.google.com/"; 
		driver.get(url);
		//*[@name='q']
		
		// locators
		//By inputText = By.xpath("//*[@name='q']"); // static 
		
		By locator1 = By.name("q");
		
				
	    WebElement var1 = driver.findElement(locator1);
		var1.sendKeys("CPSAT");
		var1.sendKeys(Keys.ENTER);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // these slow down the automation

		
		driver.quit(); // if more than one browser opened = closed

		

	}

}

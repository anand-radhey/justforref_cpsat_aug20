package day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Wikipedia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(); // driver is a chrome driver	
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String url = "https://www.wikipedia.org/"; 
		driver.get(url);
		
		By locatorEnglish = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
		
		WebElement element1 = driver.findElement(locatorEnglish);
		
		element1.click();
		
		By searchLocator = By.xpath("//*[@id=\"searchInput\"]");
		
		WebElement searchElement = driver.findElement(searchLocator);
		
		searchElement.sendKeys("Selenium");
		
		By magnifyingLocator = By.xpath("//*[@id=\"searchButton\"]");
		
		driver.findElement(magnifyingLocator).click();
		
		String title = driver.getTitle();
		
		if (title.contains("Wikipedia")) {
			System.out.println("pass");
		}
		else {
			System.out.println("fail");
			
		}
		
		driver.quit();
		
		
		

	}

}

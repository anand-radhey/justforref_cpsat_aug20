package sequencingNG;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ShowGrouping {
 
	   @Test(groups = { "sanitytest", "fullregressiontest" })
	   public void testforboth() {
	      System.out.println("Inside testforboth()");
	      
	   }

	   @Test(groups = { "sanitytest"})
	   public void testonlyforsanity() {
	      System.out.println("Inside testonlyforsanity()");
	   }

	   @Test(groups = { "sanitytest"})
	   public void test_2() {
	      System.out.println("Inside test_2()");
	   }
	   
	   @Test(groups = { "sanitytest"})
	   public void test_3() {
	      System.out.println("Inside test_3()");
	   }
	   
	   @Test(groups = { "fullregressiontest" })
	   public void testonlyforfullregression() {
	      System.out.println("Inside testonlyforfullregression()");
	   }
	   
	   @Test(groups = { "fullregressiontest" })
	   public void test_4() {
	      System.out.println("Inside test_4()");
	   }
	   
	   @Test(groups = { "fullregressiontest" })
	   public void test_5() {
	      System.out.println("Inside test_5()");
	   }
	   
	   @Test(groups = { "sanitytest", "fullregressiontest" })
	   public void testforboth_1() {
	      System.out.println("Inside testforboth_1()");
	      
	   }
	   
}

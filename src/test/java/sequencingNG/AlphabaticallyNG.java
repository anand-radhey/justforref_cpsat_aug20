package sequencingNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class AlphabaticallyNG {
	
	
	@BeforeTest
	public void setup() {
		System.out.println("Inside BeforeTest setup()");
	}
	
  @Test(groups = { "fullregressiontest" })
  public void first() {
	  System.out.println("Inside first()");
  }
  
  @Test(groups = { "sanitytest", "fullregressiontest" })
  public void second() {
	  System.out.println("Inside second()");
  }
  
  @Test(groups = { "sanitytest", "fullregressiontest" })
  public void third() {
	  System.out.println("Inside third()");
  }
  
  @Test(groups = { "sanitytest" })
  public void fourth() {
	  System.out.println("Inside fourth()");
  }
  
  @Test
  public void fifth() {
	  System.out.println("Inside fifth()");
	  fifth("Anand");
  }
  
 
  public void fifth(String y) {
	  System.out.println("Inside fifth(Str)" + y);
  }
  
  
  
	@AfterTest
	public void teardown() {
		System.out.println("Inside AfterTest teardown()");
	}
  

}

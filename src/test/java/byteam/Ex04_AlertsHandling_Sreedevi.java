package byteam;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import utils.HelperFunctions;

public class Ex04_AlertsHandling_Sreedevi {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		/*
		 * System.setProperty("webdriver.chrome.driver",
		 * "src\\test\\resources\\drivers\\chromedriver.exe"); driver = new
		 * ChromeDriver(); // driver is a chrome driver
		 * driver.manage().window().maximize();
		 * driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 */
		driver = HelperFunctions.createAppropriateDriver("chrome");
		String url = "https://the-internet.herokuapp.com/javascript_alerts";
		driver.get(url);
		System.out.println("Before Test");
	}

	@Test
	public void f() throws InterruptedException {
		System.out.println("Hello TestNG");

		// JSAlert
		By locatorJSAlert = By.xpath("//*[@id='content']/div/ul/li[1]/button");
		WebElement element1 = driver.findElement(locatorJSAlert);
		element1.click();
		Thread.sleep(5000);
		// To capture the alert message.
		driver.switchTo().alert().getText();
		// Switching to Alert and clicking on OK
		driver.switchTo().alert().accept();
		// capturing the confirmation message
		String actual_msg = driver.findElement(By.xpath("//*[@id='result']")).getText();
		String Expected_msg = "You successfuly clicked an alert";
		Assert.assertEquals(actual_msg, Expected_msg);
		System.out.println("Assert1 confirmed");

		// JSConfirm
		By locatorJSConfirm = By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button");
		WebElement element2 = driver.findElement(locatorJSConfirm);
		element2.click();
		Thread.sleep(5000);
		// To capture the alert message.
		driver.switchTo().alert().getText();
		// Switching to Alert and clicking on Cancel
		driver.switchTo().alert().dismiss();
		// capturing the confirmation message
		String actual_msg1 = driver.findElement(By.xpath("//*[@id=\'result\']")).getText();
		String Expected_msg1 = "You clicked: Cancel";
		Assert.assertEquals(actual_msg1, Expected_msg1);
		System.out.println("Assert2 confirmed");

		// JSPrompt
		By locatorJSPrompt = By.xpath("//*[@id=\"content\"]/div/ul/li[3]/button");
		WebElement element3 = driver.findElement(locatorJSPrompt);
		element3.click();
		Thread.sleep(5000);
		// To capture the alert message.
		driver.switchTo().alert().getText();
		// Switching to Alert and send some data to alert box and click on OK
		driver.switchTo().alert().sendKeys("Sree");
		driver.switchTo().alert().accept();
		// capturing the confirmation message
		String actual_msg3 = driver.findElement(By.xpath("//*[@id=\'result\']")).getText();
		String Expected_msg3 = "You entered: Sree";
		Assert.assertEquals(actual_msg3, Expected_msg3);
		System.out.println("Assert3 confirmed");

		
		
		List<WebElement>  lstwe = driver.findElements(By.xpath("//*[@id=\"custom_html-10\"]/div/ul/li[*]/b/b/a"));
		
		for( WebElement      we : lstwe) {
			String href = we.getAttribute("href");
			
		}
		
		
	}

	@AfterTest
	public void afterTest() {

		driver.close();
	}

}
package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex14_ATACalcDemo_NG {
	WebDriver driver = null;
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("http://ata123456789123456789.appspot.com/");
	  }

	
	
  @Test
  public void f_mul() {
	  int inum1 = 12;
	  int inum2 = 10;
	 int expResult = 120;
	 
	 Integer Intnum1 = inum1;
	  String strNum1 = Intnum1.toString();
	  
	  Integer Intnum2 = inum2;
	  String strNum2 = Intnum2.toString();
	  
	  Integer IntExpRes = expResult;
	  String strExpRes = IntExpRes.toString();
	  
	//*[@id="ID_nameField1"]     inputField1
	//*[@id="ID_nameField2"]     inputField2
	//*[@id="gwt-uid-2"]         radioMul
	//*[@id="ID_calculator"]     calcbutton
	//*[@id="ID_nameField3"]     resultBox
	  
	  By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
	  WebElement weField1 = driver.findElement(byField1);
	  
	  //By byField1 = By.id("ID_nameField1");
	  
	  By byField2 = By.xpath("//*[@id=\"ID_nameField2\"]");
	  WebElement weField2 = driver.findElement(byField2);
	  
	  By byMul = By.xpath("//*[@id=\"gwt-uid-2\"]") ;
	  WebElement weMul = driver.findElement(byMul);
	  
	  By byCalc = By.xpath("//*[@id=\"ID_calculator\"]") ;
	  WebElement weCalc = driver.findElement(byCalc);
	  
	  By byResult = By.xpath("//*[@id=\"ID_nameField3\"]");
	  WebElement weResult = driver.findElement(byResult);
	  
	  //Integer.parseInt("12");
	  weField1.clear();
	  weField1.sendKeys(strNum1);
	  weField2.clear();
	  weField2.sendKeys(strNum2);
	  weMul.click();
	  weCalc.click();
	  String strActRes = weResult.getAttribute("value"); //weResult.getText();
	  int iactRes = Integer.parseInt(strActRes);
	  
	  
	  //Assert.assertEquals(strActRes, strExpRes);
	  Assert.assertEquals(iactRes, expResult);
	    
	  
  }
  
  @AfterTest
  public void afterTest() {
	  //driver.quit();
  }

}

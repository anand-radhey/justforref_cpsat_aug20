package day5;

import org.testng.annotations.Test;

import utils.HelperFunctions;


import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex14_ATACalcPoMDemo {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.get("http://ata123456789123456789.appspot.com/");

	}



	@Test
	public void f() {
		Ex14_ATACalcPoM ataPage = new Ex14_ATACalcPoM(driver);
		
		int expectedResult = 120;
		String actualResult = ataPage.multiply("12", "10");
		int iActualResult = Integer.parseInt(actualResult);
		Assert.assertEquals(iActualResult, expectedResult,"Results do not match");
		
		int expectedResult1 = 18;
		String actualResult1 = ataPage.add("14", "4");
		int iActualResult1 = Integer.parseInt(actualResult1);
		Assert.assertEquals(iActualResult1, expectedResult1,"Results do not match");
		
		int expectedResult2 = 14;
		String actualResult2 = ataPage.compare("14", "10");
		int iActualResult2 = Integer.parseInt(actualResult2);
		Assert.assertEquals(iActualResult2, expectedResult2,"Comparison Failed");
		
		
	}


	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

}

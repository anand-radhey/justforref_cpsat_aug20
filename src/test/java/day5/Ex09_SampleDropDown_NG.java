package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class Ex09_SampleDropDown_NG {
	WebDriver driver = null;
	
	
	@BeforeTest
	  public void beforeTest() {
		
		driver = utils.HelperFunctions.createAppropriateDriver("chrome", false);
		
	  }
	
	
  @Test
  public void f() {
	  
	  // file:///C:/ZVA/CPSAT/cpsat131mavenproject/src/test/resources/data/dropdown.html
	  driver.get("file:///C:/ZVA/CPSAT/cpsat131mavenproject/src/test/resources/data/dropdown.html");
	  
	  
	//  /html/body/form/select
	  
	  By byvar = By.xpath("/html/body/form/select");
	  WebElement weselect = driver.findElement(byvar);
	  
	  Select selectObject = new Select(weselect);
	  
	  List<WebElement>   lstOptions =  selectObject.getOptions();    //selectObject.getAllSelectedOptions();
	  
	  for ( int i=0; i < lstOptions.size() ; i++) {
		  String strOption = lstOptions.get(i).getText();
		  System.out.println(i + ") Option is : "+ strOption);
	  }
	  
	  try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  
	  selectObject.selectByIndex(5);
	  
	  try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  selectObject.selectByValue("41");
	  
	  try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  selectObject.selectByVisibleText("CP-DOF");
	  
	  
	  
	  
	  
  }
  

  @AfterTest
  public void afterTest() {
	  
	  //driver.quit();
  }

}

package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex11_JQuery_Iframe_NG {
	WebDriver driver = null;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	}



	@Test
	public void f() {

		driver.get("https://jqueryui.com/autocomplete/");

		//*[@id="tags"]
		driver.switchTo().frame(0);

		By byvar = By.xpath("//*[@id=\"tags\"]");
		WebElement box = driver.findElement(byvar);

		box.sendKeys("j");

		List<WebElement> lstwe = driver.findElements(By.xpath("/html/body/ul/li"));

		int myIndex = 0;
		for(int i=0 ; i < lstwe.size(); i++) {
			String option =  lstwe.get(i).getText();
			System.out.println(option);
			if(option.equals("Java")) {
				//lstwe.get(i).click();
				myIndex = i;
			}

		}

		lstwe.get(myIndex).click();

		/*
	  driver.switchTo().parentFrame();
	  WebElement wesearch = driver.findElement(By.xpath("//*[@id=\"main\"]/form/label/span[2]/input"));
	  wesearch.sendKeys("Python");
		 */
	}

	@AfterTest
	public void afterTest() {
		// driver.quit()
	}

}

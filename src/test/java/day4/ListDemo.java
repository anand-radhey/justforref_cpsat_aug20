package day4;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		List<Integer> lstInt = new ArrayList<Integer>();
		lstInt.add(100);
		lstInt.add(300);
		lstInt.add(500);
		
		int n = lstInt.size();
		
		
		
		
		System.out.println(n);
		
		List<String> lstStr = new ArrayList<String>();
		lstStr.add("India");
		lstStr.add("Japan");
		lstStr.add("USA");
		lstStr.add("UK");
		
		int cnt = lstStr.size();  // ["india", "Japan", "USA", "UK"]   lstStr.get(0) = "India"
		
		for(int i = 0; i < cnt; i++) {
			String str = lstStr.get(i);
			System.out.println(str);
		}
		
		for(  String      ele   : lstStr) {
			System.out.println(ele);
		}
		
		
		
		/*
		List<String> lstStr = new ArrayList<String>();
		lstStr.add("India");
		lstStr.add("Japan");
		lstStr.add("USA");
		lstStr.add("UK");
		
		int cnt = lstStr.size();
		
		
		for (int i=0; i < cnt ; i++ ) {
			System.out.println(lstStr.get(i));
		}
		
		for ( String ele     : lstStr) {
			System.out.println(ele);
		}

	
*/
	}
}

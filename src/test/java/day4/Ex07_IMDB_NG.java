package day4;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_NG {
	
	WebDriver driver = null;
	
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("ChroME");
		
	  }

	
  @Test
  public void f() {
	  String movie = "Baazigar";
	  String expDir = "Mastan";
	  String expStar = "Shah Rukh Khan";
	  
	  driver.get("https://www.imdb.com/");
	//*[@id="suggestion-search"]      //Search Box
	  WebElement searchBox = driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]"));
	  searchBox.sendKeys(movie);
	  searchBox.sendKeys(Keys.ENTER);
	  
	//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a     movie Link
	
	  
	  driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();
	  
	  
	//*[@id="title-overview-widget"]/div[2]/div[1]/div[2]/h4     Directors:
	  
	//*[@class='inline'  and contains( text()  , 'Director'   )]/following-sibling::*
	  By bydir = By.xpath("//*[@class='inline' and contains(text(),'Director')]/following-sibling::*");
	  
	  List<WebElement>  lstDir =    driver.findElements(bydir);
	  
	  boolean dirFound = false; 
	  for( WebElement       ele : lstDir) {
		  String actDir = ele.getText();
		  
		  
		  if(actDir.contains(expDir)) {
			  System.out.println("Director Match Found : " + "\nActual Dir is : "+ actDir + " :: And Exp Dir is : "+ expDir);
			  dirFound = true;
			  break;
		  }
		  
		  
		  
	  }
	  
	  
	  
 By bystar = By.xpath("//*[@class='inline' and contains(text(),'Star')]/following-sibling::*");
	  
	  List<WebElement>  lstStar =    driver.findElements(bystar);
	  
	  boolean starFound = false; 
	  for( WebElement       ele : lstStar) {
		  String actStar = ele.getText();
		  
		  
		  if(actStar.contains(expStar)) {
			  System.out.println("Star Match Found : " + "\nActual Star is : "+ actStar + " :: And Exp Star is : "+ expStar);
			  starFound = true;
			  break;
		  }
		  
		  
		  
	  }
	  
	  
	  
	  
	  
	  
	  
	  
	  
  }
  
  
  
  
   
  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}

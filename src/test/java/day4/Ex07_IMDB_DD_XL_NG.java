package day4;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Ex07_IMDB_DD_XL_NG {

	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("ChroME");

	}


	@DataProvider
	public Object[][] mydp() throws EncryptedDocumentException, InvalidFormatException, IOException {
/*
		String[][] data = { {"Baazigar","Mastan",  "Shah Rukh Khan"  },
				{"Uri","Aditya",  "Vicky"  }, 
				{"Raazi","Meghna",  "Vicky"  },		};
		*/
		
		
		String[][] data = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\imdbdata.xlsx", "data");
		

		return data;

	}

	@Test(dataProvider = "mydp")
	public void f(String v1, String v2, String v3) {
		String movie = v1 ;   // "Baazigar";
		String expDir = v2;    //"Mastan";
		String expStar = v3;   //"Shah Rukh Khan";

		System.out.println("v1: "+v1+"  v2: "+ v2 + " v3: "+v3);

		driver.get("https://www.imdb.com/");
		//*[@id="suggestion-search"]      //Search Box
		WebElement searchBox = driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]"));
		searchBox.sendKeys(movie);
		searchBox.sendKeys(Keys.ENTER);

		//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a     movie Link


		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();


		//*[@id="title-overview-widget"]/div[2]/div[1]/div[2]/h4     Directors:

		//*[@class='inline'  and contains( text()  , 'Director'   )]/following-sibling::*
		By bydir = By.xpath("//*[@class='inline' and contains(text(),'Director')]/following-sibling::*");

		List<WebElement>  lstDir =    driver.findElements(bydir);

		boolean dirFound = false; 
		for( WebElement       ele : lstDir) {
			String actDir = ele.getText();


			if(actDir.contains(expDir)) {
				System.out.println("Director Match Found : " + "\nActual Dir is : "+ actDir + " :: And Exp Dir is : "+ expDir);
				dirFound = true;
				break;
			}

		}


		By bystar = By.xpath("//*[@class='inline' and contains(text(),'Star')]/following-sibling::*");

		List<WebElement>  lstStar =    driver.findElements(bystar);

		boolean starFound = false; 
		for( WebElement       ele : lstStar) {
			String actStar = ele.getText();


			if(actStar.contains(expStar)) {
				System.out.println("Star Match Found : " + "\nActual Star is : "+ actStar + " :: And Exp Star is : "+ expStar);
				starFound = true;
				break;
			}

		}


	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}

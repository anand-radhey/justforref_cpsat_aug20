package day4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex06_SocialMedia_NG {
	
	WebDriver driver= null;
	
	 @BeforeTest
	  public void beforeTest() {
		 driver = utils.HelperFunctions.createAppropriateDriver("chrome", true);
	  }
	
	
  @Test
  public void f() {
	  
	  driver.get("http://agiletestingalliance.org/");
	  
	 // By popupclose = By.xpath("//*[@id=\"elementor-popup-modal-367\"]/div/div[4]/i");
		
	//	WebElement element = driver.findElement(popupclose);
	//	element.click();
	  
	  
	  
	  //By byvar1 = By.xpath("//*[@id=\"elementor-popup-modal-11945\"]/div/div[4]/i");
	  driver.findElement(By.xpath("//*[@id=\"elementor-popup-modal-11945\"]/div/div[4]/i")).click();
		
		String filename = "src\\test\\resources\\screenshots\\imageSocialMediaheadless.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
		
		//*[@id="custom_html-10"]/div/ul/li[1]/b/b/a
		//*[@id="custom_html-10"]/div/ul/li[1]/b/b/a
		//*[@id="custom_html-10"]/div/ul/li[2]/b/b/a
		//*[@id="custom_html-10"]/div/ul/li[3]/b/b/a
		//*[@id="custom_html-10"]/div/ul/li[4]/b/b/a
		//*[@id="custom_html-10"]/div/ul/li[5]/b/b/a
		By byvar = By.xpath("//*[@id='custom_html-10']/div/ul/li[*]/b/b/a");
		List<WebElement> lstwe = driver.findElements(byvar);
		
		for( WebElement      we  :lstwe ) {
			String hrefstr = we.getAttribute("href");
			System.out.println(hrefstr);
		}
		
		
		
	  
  }
 

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}

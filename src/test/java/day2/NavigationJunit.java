package day2;

 

import static org.junit.Assert.*;

 

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


import utils.HelperFunctions;

 

public class NavigationJunit {

 

    WebDriver driver;
    
    @Before
    public void setUp() throws Exception {
        driver=HelperFunctions.createAppropriateDriver("firefox");
        driver.get("https://www.google.com/");
    }

 

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

 

    @Test
    public void test() {
    	
        driver.navigate().to("https://www.ataevents.org/");
        driver.navigate().back();
        System.out.println(driver.getTitle());
        driver.navigate().forward();
        System.out.println(driver.getTitle());
        driver.navigate().forward();
    }

 

}
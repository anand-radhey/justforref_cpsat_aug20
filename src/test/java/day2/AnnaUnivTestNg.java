package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class AnnaUnivTestNg {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");		
		/*
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(); // driver is a chrome driver	
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 */
		driver = HelperFunctions.createAppropriateDriver("chrome");

		String url = "https://www.annauniv.edu/"; 
		driver.get(url);
	}	

	@Test
	public void f() throws InterruptedException {


		WebElement ele = driver.findElement(By.xpath("//a[contains(text(),'Departments')]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", ele);
		// //*[@id="link3"]/strong
		Actions actionObject = new Actions(driver);
		// actions class for MouseOver, contextClicks, doubleclicks, drag and drop
		actionObject.moveToElement(driver.findElement(By.xpath("//*[@id='link3']/strong"))).perform();
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		
		By oceanManagementLocator = By.xpath("//*[@id=\"menuItemHilite32\"]");
		
		/*
		try {

			// AJAX elements ? JQuery 
			wait.until(ExpectedConditions.elementToBeClickable(oceanManagementLocator));
			
		}
		catch(Exception e) {
			
			System.out.println(e.getMessage());
		}
		*/
		
		actionObject.moveToElement(driver.findElement(oceanManagementLocator)).click().perform();
		
		//driver.findElement(By.xpath("//*[@id=\"menuItemHilite32\"]")).click();
		System.out.println(driver.getTitle());

	}


	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}

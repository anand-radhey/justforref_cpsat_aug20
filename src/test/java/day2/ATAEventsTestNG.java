package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class ATAEventsTestNG {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");		
		/*
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(); // driver is a chrome driver	
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 */
		driver = HelperFunctions.createAppropriateDriver("chrome");

		String url = "https://www.ataevents.org/"; 
		driver.get(url);
	}


	@Test
	public void f() throws InterruptedException {
		
		// //*[@id="elementor-popup-modal-367"]/div/div[4]/i
		
		By popupclose = By.xpath("//*[@id=\"elementor-popup-modal-367\"]/div/div[4]/i");
		
		WebElement element = driver.findElement(popupclose);
		element.click();
		
		//a[contains(text(),'CP-DOF')]
		
		driver.findElement(By.xpath("//a[contains(text(),'CP-DOF')]")).click();

		Thread.sleep(4000);
		
		System.out.println(driver.getTitle());
		
		// multiple windows - how to switch to new a window
		HelperFunctions.switchToWindow(driver, "CP-DOF");
		
		System.out.println(driver.getTitle());
		
		// multiple windows - how to switch to new a window
		HelperFunctions.switchToWindow(driver, "Best");
		
		System.out.println(driver.getTitle());
		
		
	}


	@AfterTest
	public void afterTest() {
		
		driver.quit();
	}

}

package tablesdata;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DataTablesPoM {

	WebDriver driver;
	
	// By objects
	By byTableSpanRows = By.xpath("//*[@id=\"example_paginate\"]/span/a");
	By tableRows = By.xpath("//*[@id=\"example\"]/tbody/tr");
	
	List<String> col1List = new ArrayList <String>();
	List<String> col2List = new ArrayList <String>();
	List<String> col3List = new ArrayList <String>();
	List<String> col4List = new ArrayList <String>();

	WebDriverWait wait;
	
	public DataTablesPoM(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		wait = new WebDriverWait(driver,10);
		
	}

	public void readAlltheTable() {
		
		List<WebElement> listofSpans = driver.findElements(byTableSpanRows);
		
		int spanSize = listofSpans.size();
		System.out.println("number of table pages to click = " + spanSize );
		
		for (int i = 2 ; i <= spanSize ; i++)
		{
			System.out.println("Reading rows for span = " + (i-1));
			readTable();
			System.out.println("clicking on span = " + (i));
			By nextSpan = By.xpath("//*[@id=\"example_paginate\"]/span/a["+(i)+"]");
			driver.findElement(nextSpan).click();
		}

		// last remaining one remaining
		System.out.println("Reading rows for span = " + spanSize);
		readTable();

	
	}
	
	public void readTable() {
		
		// read page 1
		
		//*[@id="example"]/tbody/tr/
		

		int sizeOfTable=0;
		try {
			List<WebElement> listOfRows = driver.findElements(tableRows);
			wait.until(ExpectedConditions.visibilityOfAllElements(listOfRows));
			sizeOfTable = listOfRows.size();
			System.out.println("Table rows = " + sizeOfTable);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		for (int i = 1; i <=sizeOfTable;i++ ) {
			
			List<WebElement> listofColumns = driver.findElements(By.xpath("//*[@id=\"example\"]/tbody/tr["+i+"]/td"));

			System.out.println(listofColumns.size());

			for (int j = 0; j <listofColumns.size();j++ ) {
				
				String columnValue = listofColumns.get(j).getText();
				
				if (j==0) {
					col1List.add(columnValue);
				}
				else if (j==1) {
					col2List.add(columnValue);
					
				}
				else if (j==2) {
					col3List.add(columnValue);
					
				}
				else if (j==3) {
					col4List.add(columnValue);
					
				}				
			}
		}
	}
	
	public boolean searchFunction(String param1, 
			String param2, 
			String param3, 
			String param4) {
		
		boolean flag = false;
		
		for (int i = 0; i <col1List.size();i++ ) {
			
			System.out.println("column 1 Value =" + "i = " + i + "value = "+ col1List.get(i));
			
			if (param1.equals(col1List.get(i))) {
				System.out.println("1 colmn matched" + col1List.get(i));
				System.out.println("cl2" + col2List.get(i));
				System.out.println("col3" + col3List.get(i));
				System.out.println("col4" + col4List.get(i));
				
				if (param2.equals(col2List.get(i))) {
					if (param3.equals(col3List.get(i))) {
						if (param4.equals(col4List.get(i))) {
						
							flag = true;
							System.out.println("match found");
						}
					}
				}
			}
		}
		
		return flag;
	}
	
}

package tablesdata;

import org.testng.annotations.Test;
import org.testng.Assert;

import utils.DataReaders;
import utils.HelperFunctions;
import utils.DataWriter;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class Ex20_DataDrivenPomExcelReadWrite_NG {

	WebDriver driver;
	List<String> col1List = new ArrayList <String>();
	List<String> col2List = new ArrayList <String>();
	List<String> col3List = new ArrayList <String>();
	List<String> col4List = new ArrayList <String>();

	String fileName = "src/test/resources/data/TablesTestData.xlsx";
	String outputFileName = "src/test/resources/data/Result.xlsx";
	
	String inputSheet = "Sheet1";
	String outputSheet = "Sheet2";


	DataTablesPoM pomObject;

	@Parameters({"Browser","param2","param2"})
	@BeforeTest
	public void beforeTest( @Optional("chrome") String brwser,
			@Optional("1") String p2,
			@Optional("2") String p3) {

		driver = HelperFunctions.createAppropriateDriver(brwser);
		String url = "https://datatables.net/examples/styling/jqueryUI.html"; 
		driver.get(url);

		pomObject = new DataTablesPoM(driver);
		pomObject.readAlltheTable(); // all the 4 lists

		//pomObject.readTable();



	}


	@Test(dataProvider = "dp", enabled = true)
	public void f(String param1,
			String param2,
			String param3,
			String param4) {

		System.out.println("parameters are" + param1 + param2 + param3 + param4);

		boolean flag = pomObject.searchFunction(param1, param2, param3,param4);

		System.out.println("Row was matched = " + flag);

		ArrayList<String> list=new ArrayList<String>();//Creating arraylist    
		list.add(param1);//Adding object in arraylist    
		list.add(param2);    
		list.add(param3);    
		list.add(param4);    

		try {
			DataWriter.writeToFile(outputFileName, outputSheet,list,flag);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(flag, true, "Rows did match");

	}


	@DataProvider
	public Object[][] dp() throws IOException, EncryptedDocumentException, InvalidFormatException {

		//String[][] data =	DataReaders.getExcelDataUsingPoi(fileName, inputSheet);
		String[][] data =	utils.XLDataReaders.getExcelData(fileName, inputSheet);
			
		return data;

	}

	@AfterTest
	public void afterTest() {

		driver.quit();
	}


}
